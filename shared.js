const backdrop = document.querySelector('.backdrop');
const selectPlanButtons = document.querySelectorAll('.plan button');
const modal = document.querySelector('.modal');
const modalNoButton = document.querySelector('.modal__action.modal__action--negative');
const hamburgerMenu = document.querySelector('.toggle-button');
const mobileNav = document.querySelector('.mobile-nav');
const ctaButton = document.querySelector('.main-nav__item--cta');

for (let i = 0; i < selectPlanButtons.length; i++) {
    selectPlanButtons[i].addEventListener('click', () => {
        backdrop.style.display = 'block';
        setTimeout(() => {
            backdrop.classList.add('open');
        }, 10);
        modal.style.display = 'block';
        setTimeout(() => {
            modal.classList.add('open');
        }, 10);
    });
}

backdrop.addEventListener('click', () => {
    closeModal();
    mobileNav.classList.remove('open');
});

if (modalNoButton) {
    modalNoButton.addEventListener('click', closeModal);
}

function closeModal() {
    backdrop.classList.remove('open');
    setTimeout(() => {
        backdrop.style.display = 'none';
    }, 400);
    if (modal) {
        modal.classList.remove('open');
        setTimeout(() => {
            modal.style.display = 'none';
        }, 400);
    }
}

hamburgerMenu.addEventListener('click', () => {
    backdrop.style.display = 'block';
    setTimeout(() => {
        backdrop.classList.add('open');
    }, 10);
    mobileNav.classList.add('open');
});

ctaButton.addEventListener('animationstart', event => {
    console.log('Animation started', event);
})

ctaButton.addEventListener('animationiteration', event => {
    console.log('Animation iteration', event);
})

ctaButton.addEventListener('animationend', event => {
    console.log('Animation ended', event);
})
